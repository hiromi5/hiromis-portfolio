<?php

function hiromi_header($title){

	$header = <<<_HTML

	<title>$title</title>
	<link rel="stylesheet" type="text/css" href="css/mystyle.css">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
_HTML;

echo $header;

}


function hiromi_nav($title){

	$home_bt = $about_bt = $gallery_bt = $portfolio_bt = '';

	switch ($title) {
		case 'Home':
			$home_bt = "style='color: rgba(62, 98, 255, 1)!important; text-shadow: 5px 5px 5px #5effe2;'";
			break;
		
		case 'About me':
			$about_bt = "style='color: rgba(62, 98, 255, 1)!important; text-shadow: 5px 5px 5px #5effe2;'";
			break;
		case 'Gallery':
			$gallery_bt = "style='color: rgba(62, 98, 255, 1)!important; text-shadow: 5px 5px 5px #5effe2;'";
			break;
		case 'Portfolio':
			$portfolio_bt = "style='color: rgba(62, 98, 255, 1)!important; text-shadow: 5px 5px 5px #5effe2;'";
			break;
	}

	$nav = <<<_HTML

	<div class="container-fluid" >
	<div class="col-sm-2"><a href="index.php" $home_bt>Home</a></div>
	<div class="col-sm-2"><a href="aboutme.php" $about_bt>About me</a></div>
	<div class="col-sm-2"><a href="gallery.php" $gallery_bt>Gallery</a></div>
	<div class="col-sm-2"><a href="portfolio.php" $portfolio_bt>Portfolio</a></div>
	</div>
_HTML;

echo $nav;
}

function sankaku(){

	$sankaku = <<<_HTML
		<div class="sankakuleft"></div>
		<div class="sankakuleft2"></div>
		<div class="sankakuleft3"></div>
		<div class="sankakuleft4"></div>
		<div class="sankakuleft5"></div>
		<div class="sankakuleft6"></div>
		<div class="sankakuleft7"></div>
		<div class="sankakuleft9"></div>
		<div class="sankakuleft10"></div>
		<div class="sankakuleft11"></div>
		<div class="sankakuleft12"></div>
		<div class="sankakuleft13"></div>
		<div class="sankakuleft14"></div>

		<div class="sankakuright"></div>
		<div class="sankakuright2"></div>
		<div class="sankakuright3"></div>
		<div class="sankakuright4"></div>
		<div class="sankakuright5"></div>
		<div class="sankakuright6"></div>
		<div class="sankakuright7"></div>
		<div class="sankakuright9"></div>
		<div class="sankakuright10"></div>
		<div class="sankakuright11"></div>
		<div class="sankakuright12"></div>
		<div class="sankakuright13"></div>
		<div class="sankakuright14"></div>
_HTML;

echo $sankaku;
}

function title_name($t_name){
	$tn = <<<_HTML
		<h1 class="about-name">$t_name</h1>
_HTML;

echo $tn;
}
?>