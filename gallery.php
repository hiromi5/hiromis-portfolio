﻿<?php 
require('header.php');
?>
<!DOCTYPE html>
<html>
<head>
<?php hiromi_header('Gallery'); ?>
    <link rel="stylesheet" type="text/css" href="css/slider.css">
    <script src="js/jssor.slider-23.1.5.min.js" type="text/javascript"></script>
  	<script src="js/slider.js" type="text/javascript"></script>

</head>
<body>
<div class="body2">
<?php sankaku(); ?>


	<div class="article">
	<?php title_name('Gallery'); ?>
	
	</div>

<?php hiromi_nav('Gallery'); ?>

<div class="article">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="js/jquery.bxslider.min.js"></script>
    <link href="css/jquery.bxslider.css" rel="stylesheet" />
    <script type="text/javascript">
            $(document).ready(function(){
                $('.bxslider').bxSlider({
                    auto: true,
                });
        	});
    </script>

    
<div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:960px;height:480px;overflow:hidden;visibility:hidden;background-color:#24262e;">
        <!-- Loading Screen -->
        <div data-u="loading" style="position:absolute;top:0px;left:0px;background-color:rgba(0,0,0,0.7);">
            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
            <div style="position:absolute;display:block;background:url('img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
        </div>
        <div data-u="slides" style="cursor:default;position:relative;top:0px;left:240px;width:720px;height:480px;overflow:hidden;">
            <div>
                <img data-u="image" src="img/IMG_0013.JPG" />
                <img data-u="thumb" src="img/IMG_0013.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_0333.JPG" />
                <img data-u="thumb" src="img/IMG_0333.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_1289.JPG" />
                <img data-u="thumb" src="img/IMG_1289.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_1313.JPG" />
                <img data-u="thumb" src="img/IMG_1313.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_1346.JPG" />
                <img data-u="thumb" src="img/IMG_1346.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/FullSizeRender 11.jpg" />
                <img data-u="thumb" src="img/FullSizeRender 11.jpg" />
            </div>
            <div>
                <img data-u="image" src="img/FullSizeRender 10.jpg" />
                <img data-u="thumb" src="img/FullSizeRender 10.jpg" />
            </div>
            <div>
                <img data-u="image" src="img/FullSizeRender 9.jpg" />
                <img data-u="thumb" src="img/FullSizeRender 9.jpg" />
            </div>
            <div>
                <img data-u="image" src="img/FullSizeRender 8.jpg" />
                <img data-u="thumb" src="img/FullSizeRender 8.jpg" />
            </div>
            <div>
                <img data-u="image" src="img/FullSizeRender 7.jpg" />
                <img data-u="thumb" src="img/FullSizeRender 7.jpg" />
            </div>
            <div>
                <img data-u="image" src="img/FullSizeRender 6.jpg" />
                <img data-u="thumb" src="img/FullSizeRender 6.jpg" />
            </div>
            <div>
                <img data-u="image" src="img/FullSizeRender 5.jpg" />
                <img data-u="thumb" src="img/FullSizeRender 5.jpg" />
            </div>
            <div>
                <img data-u="image" src="img/FullSizeRender 3.jpg" />
                <img data-u="thumb" src="img/FullSizeRender 3.jpg" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_1348.JPG" />
                <img data-u="thumb" src="img/IMG_1348.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_1897.JPG" />
                <img data-u="thumb" src="img/IMG_1897.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_1933.JPG" />
                <img data-u="thumb" src="img/IMG_1933.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_2010.JPG" />
                <img data-u="thumb" src="img/IMG_2010.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_2050.JPG" />
                <img data-u="thumb" src="img/IMG_2050.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_2205.JPG" />
                <img data-u="thumb" src="img/IMG_2205.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_2208.JPG" />
                <img data-u="thumb" src="img/IMG_2208.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_2209.JPG" />
                <img data-u="thumb" src="img/IMG_2209.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_2210.JPG" />
                <img data-u="thumb" src="img/IMG_2210.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_2211.JPG" />
                <img data-u="thumb" src="img/IMG_2211.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_2246.JPG" />
                <img data-u="thumb" src="img/IMG_2246.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_2256.JPG" />
                <img data-u="thumb" src="img/IMG_2256.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_2260.JPG" />
                <img data-u="thumb" src="img/IMG_2260.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_2261.JPG" />
                <img data-u="thumb" src="img/IMG_2261.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_2320.JPG" />
                <img data-u="thumb" src="img/IMG_2320.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_2329.JPG" />
                <img data-u="thumb" src="img/IMG_2329.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_2366.JPG" />
                <img data-u="thumb" src="img/IMG_2366.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_2390.JPG" />
                <img data-u="thumb" src="img/IMG_2390.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_2432.JPG" />
                <img data-u="thumb" src="img/IMG_2432.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_2441.JPG" />
                <img data-u="thumb" src="img/IMG_2441.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_2590.JPG" />
                <img data-u="thumb" src="img/IMG_2590.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_2618.JPG" />
                <img data-u="thumb" src="img/IMG_2618.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_2759.JPG" />
                <img data-u="thumb" src="img/IMG_2759.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_2773.JPG" />
                <img data-u="thumb" src="img/IMG_2773.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_2785.JPG" />
                <img data-u="thumb" src="img/IMG_2785.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_2796.JPG" />
                <img data-u="thumb" src="img/IMG_2796.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_2803.JPG" />
                <img data-u="thumb" src="img/IMG_2803.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_2884.JPG" />
                <img data-u="thumb" src="img/IMG_2884.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_3241.JPG" />
                <img data-u="thumb" src="img/IMG_3241.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_3355.JPG" />
                <img data-u="thumb" src="img/IMG_3355.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_3448.JPG" />
                <img data-u="thumb" src="img/IMG_3448.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_3451.JPG" />
                <img data-u="thumb" src="img/IMG_3451.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_3497.JPG" />
                <img data-u="thumb" src="img/IMG_3497.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_3512.JPG" />
                <img data-u="thumb" src="img/IMG_3512.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_3583.JPG" />
                <img data-u="thumb" src="img/IMG_3583.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_3585.JPG" />
                <img data-u="thumb" src="img/IMG_3585.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_3681.JPG" />
                <img data-u="thumb" src="img/IMG_3681.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_3795.JPG" />
                <img data-u="thumb" src="img/IMG_3795.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_4344.JPG" />
                <img data-u="thumb" src="img/IMG_4344.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_4367.JPG" />
                <img data-u="thumb" src="img/IMG_4367.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_4492.JPG" />
                <img data-u="thumb" src="img/IMG_4492.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_4539.JPG" />
                <img data-u="thumb" src="img/IMG_4539.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_4806.JPG" />
                <img data-u="thumb" src="img/IMG_4806.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_5006.JPG" />
                <img data-u="thumb" src="img/IMG_5006.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_5071.JPG" />
                <img data-u="thumb" src="img/IMG_5071.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_5125.JPG" />
                <img data-u="thumb" src="img/IMG_5125.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_5140.JPG" />
                <img data-u="thumb" src="img/IMG_5140.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_5260.JPG" />
                <img data-u="thumb" src="img/IMG_5260.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_5544.JPG" />
                <img data-u="thumb" src="img/IMG_5544.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_5574.JPG" />
                <img data-u="thumb" src="img/IMG_5574.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_5619.JPG" />
                <img data-u="thumb" src="img/IMG_5619.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_5626.JPG" />
                <img data-u="thumb" src="img/IMG_5626.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_5822.JPG" />
                <img data-u="thumb" src="img/IMG_5822.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_5968.JPG" />
                <img data-u="thumb" src="img/IMG_5968.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_5998.JPG" />
                <img data-u="thumb" src="img/IMG_5998.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_6006.JPG" />
                <img data-u="thumb" src="img/IMG_6006.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_5006.JPG" />
                <img data-u="thumb" src="img/IMG_5006.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_6008.JPG" />
                <img data-u="thumb" src="img/IMG_6008.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_6055.JPG" />
                <img data-u="thumb" src="img/IMG_6055.JPG" />
            </div>
            <div>
                <img data-u="image" src="img/IMG_6120.JPG" />
                <img data-u="thumb" src="img/IMG_6120.JPG" />
            </div>






        </div>
        <!-- Thumbnail Navigator -->
        <div data-u="thumbnavigator" class="jssort01-99-66" style="position:absolute;left:0px;top:0px;width:240px;height:480px;" data-autocenter="2">
            <!-- Thumbnail Item Skin Begin -->
            <div data-u="slides" style="cursor: default;">
                <div data-u="prototype" class="p">
                    <div class="w">
                        <div data-u="thumbnailtemplate" class="t"></div>
                    </div>
                    <div class="c"></div>
                </div>
            </div>
            <!-- Thumbnail Item Skin End -->
        </div>
        <!-- Arrow Navigator -->
        <span data-u="arrowleft" class="jssora05l" style="top:0px;left:248px;width:40px;height:40px;" data-autocenter="2"></span>
        <span data-u="arrowright" class="jssora05r" style="top:0px;right:8px;width:40px;height:40px;" data-autocenter="2"></span>
    </div>
    <script type="text/javascript">jssor_1_slider_init();</script>
    <!-- #endregion Jssor Slider End -->



    

	<ul class="category">
		<li><a href="mylife.php">My life</a></li>
		<li><a href="#">Food</a></li>
		<li><a href="#">View</a></li>
	</ul>

	</div>
</div>
</body>
</html>